const express = require("express");
const router = express.Router();
const { addPost, getPosts, deletePost, getPost, updatePost } = require("../controllers/postController");

router.get("/", getPosts);
router.get("/:id", getPost);

router.post("/", addPost);

router.put("/:id", updatePost);

router.delete("/:id", deletePost);

module.exports = router;
